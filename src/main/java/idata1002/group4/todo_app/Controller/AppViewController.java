package idata1002.group4.todo_app.Controller;

import idata1002.group4.todo_app.Tasks.ListRegister;
import idata1002.group4.todo_app.Tasks.Task;
import idata1002.group4.todo_app.Tasks.TaskList;
import java.net.URL;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.util.StringConverter;

public class AppViewController implements Initializable {

  @FXML
  private ComboBox selectListComboBox;
  @FXML
  private Label emptyListNameLabel;
  @FXML
  private AnchorPane addTaskAnchorPane;
  @FXML
  private Button clearTasksButton;
  @FXML
  private Text listNameText;
  @FXML
  private TextField newListTextField;
  @FXML
  private Button createListButton;
  @FXML
  private SplitPane splitPane;
  @FXML
  private Button deleteButton;
  @FXML
  private MenuItem darkModeButton;
  @FXML
  private TableView<Task> taskListTableView;
  @FXML
  private TableColumn<Task, String> selectTaskTableView;
  @FXML
  private TableColumn<Task, String> deleteTaskTableView;
  @FXML
  private TableColumn<Task, String> deadlineTableView;
  @FXML
  private TableColumn<Task, String> taskTableView;
  @FXML
  private DatePicker deadlineDatePicker;
  @FXML
  private TextField insertTaskField;
  @FXML
  private Button addToListButton;
  private TaskList activeList;
  private ObservableList<String> menuLists;
  private ListRegister lists;


  @Override
  public void initialize(URL url, ResourceBundle resourceBundle) {

    this.lists = new ListRegister();
    this.menuLists = FXCollections.observableArrayList();

    this.splitPane.setStyle("-fx-box-border: transparent;");

    // Setup table view
    setCellValueFactory();
    // Hide table view until list is selected or created
    toggleTableView(false);
  }

  /**
   * Adds a task to the active list if the text entered in the text field is not blank.
   *
   * @param actionEvent         YASSSS
   */
  public void addToList(ActionEvent actionEvent) {
    String taskDesc = this.insertTaskField.getText();
    // Set date to format 'yyyy-mm-dd'
    String formattedDate = formatDate();

    Task task;
    // Check if date has been selected
    if (!(formattedDate.isBlank())) {
      task = new Task(taskDesc, formattedDate);
    } else {
      // Add task without date if not selected
      task = new Task(taskDesc);
    }
    // Add task if task description has been entered
    if (!(task.getTaskName().isBlank())) {
      this.activeList.addTask(task);
    }
    // Enable button when task has been added
    this.deleteButton.setDisable(false);
    // Clear text field after task added
    this.insertTaskField.clear();
    // Updates the tableview
    updateTableView();
  }

  /**
   * Converts the date from the date picker from 'dd.mm.yyyy' to 'yyyy-mm-dd' to match the parameter
   * set in the Task class' constructor.
   *
   * @return Date in format 'yyyy-mm-dd'
   */
  public String formatDate() {
    return this.deadlineDatePicker.getEditor().getText();
  }


  /**
   * Deletes the row currently selected from the active list.
   *
   * @param actionEvent       YASSSS
   */
  public void deleteSelected(ActionEvent actionEvent) {
    this.taskListTableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    // Get selected row
    ObservableList<Task> selectedTasks;
    selectedTasks = taskListTableView.getSelectionModel().getSelectedItems();
    // Delete selected row
    if (!selectedTasks.isEmpty()) {
      Task task = selectedTasks.get(0);
      this.activeList.removeTask(task);
    }

    updateTableView();
  }

  /**
   * Creates a new list and adds it to the list register.
   *
   * @param actionEvent       YASSSS
   */
  public void createNewList(ActionEvent actionEvent) {
    String listName = this.newListTextField.getText();
    // Display error message if empty text is entered
    if (this.newListTextField.getText().isBlank()) {
      this.emptyListNameLabel.setVisible(true);
    } else {
      this.activeList = new TaskList(listName);
      // Add new list to register of lists
      this.lists.addList(this.activeList);
      // Add new list to menu lists
      this.menuLists.add(this.activeList.getListName());
      this.selectListComboBox.setItems(this.menuLists);
      // Set text to header label
      this.listNameText.setText(this.activeList.getListName());
      // Display table view when list has been created
      toggleTableView(true);
      updateTableView();
      // Disable delete button until task has been added
      this.emptyListNameLabel.setVisible(false);
      // Clear text field when added list
      this.newListTextField.clear();
    }
  }

  /**
   * Shows/hides the table view.
   * Shown when list has been selected or created.
   * Hidden if no list has been selected or created.
   *
   * @param view    YASSSS
   */
  public void toggleTableView(boolean view) {
    this.taskListTableView.setVisible(view);
    this.deleteButton.setVisible(view);
    this.clearTasksButton.setVisible(view);
    this.addTaskAnchorPane.setVisible(view);
  }

  /**
   * Sets cell value factory to table view.
   */
  public void setCellValueFactory() {
    this.taskTableView.setCellValueFactory(new PropertyValueFactory<>("taskName"));
    this.deadlineTableView.setCellValueFactory(new PropertyValueFactory<>("endDate"));
  }

  /**
   * Switches table view when user clicks on a different list in the drop-down menu.
   *
   * @param actionEvent       YASSSS
   */
  public void selectList(ActionEvent actionEvent) {
    String selectedList = this.selectListComboBox.getValue().toString();

    this.activeList = this.lists.findList(selectedList);

    updateTableView();
  }

  /**
   * Sets items to table view and text to list name header depending on the chosen active list.
   * Enables/disables delete task button if list is empty or not.
   */
  public void updateTableView() {
    // Set tasks to table view
    this.taskListTableView.setItems(this.activeList.getTasks());
    this.listNameText.setText(this.activeList.getListName());
    this.selectListComboBox.setPromptText(this.activeList.getListName());
    setDatePickerFormat();
    // Disable delete button if active list is empty
    if (this.activeList.getTasks().isEmpty()) {
      this.deleteButton.setDisable(true);
    } else {
      this.deleteButton.setDisable(false);
    }
  }

  /**
   * Removes the selected task from teh task list.
   * @param actionEvent       YASSSS
   */
  public void clearSelectedTasks(ActionEvent actionEvent) {
  }

  /**
   * Sets the wanted format for the Datepicker.
   */
  private void setDatePickerFormat() {
    this.deadlineDatePicker.setConverter(new StringConverter<LocalDate>() {
      private final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

      @Override
      public String toString(LocalDate localDate) {
        if (localDate == null) {
          return "";
        }
        return dateTimeFormatter.format(localDate);
      }

      @Override
      public LocalDate fromString(String dateString) {
        if (dateString == null || dateString.trim().isEmpty()) {
          return null;
        }
        return LocalDate.parse(dateString, dateTimeFormatter);
      }
    });
    // Clear the DatePicker field after to make sure its empty
    this.deadlineDatePicker.getEditor().clear();
  }
}
