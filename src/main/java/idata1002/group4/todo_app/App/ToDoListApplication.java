package idata1002.group4.todo_app.App;

import java.net.URL;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;


public class ToDoListApplication extends Application {
  private final float appVersion = 1.00f;

  /**
   * Starts the application.
   *
   * @param args arguments.
   */
  public static void main(String[] args) {
    launch(args);
  }

  @Override
  public void start(Stage primaryStage) throws Exception {
    URL url = getClass().getClassLoader().getResource("AppView.fxml");
    assert url != null;
    Parent root = FXMLLoader.load(url);
    Scene scene = new Scene(root);
    primaryStage.setTitle("ToDoList App " + javaVersion() + " JavaFX " + javafxVersion());
    primaryStage.setScene(scene);
    primaryStage.setResizable(true);
    primaryStage.show();
  }

  @Override
  public void stop() {
    System.exit(0);
  }

  /**
   * Returns the java version used.
   *
   * @return The used java version.
   */
  public static String javaVersion() {
    return System.getProperty("java.version");
  }

  /**
   * Returns the javafx version running.
   *
   * @return The javafx version running.
   */
  public static String javafxVersion() {
    return System.getProperty("javafx.version");
  }


}
