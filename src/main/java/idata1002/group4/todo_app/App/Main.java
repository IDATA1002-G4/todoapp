package idata1002.group4.todo_app.App;

public class Main {

  /**
   * Starts the application.
   *
   * @param args Arguments.
   */
  public static void main(String[] args) {
    ToDoListApplication.main(args);
  }
}
