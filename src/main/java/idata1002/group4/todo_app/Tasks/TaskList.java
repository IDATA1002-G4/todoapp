package idata1002.group4.todo_app.Tasks;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * This Class hold information about Task Lists,
 * Name of the list and the task it holds.
 */

public class TaskList {
  private String listName;
  private ObservableList<Task> tasks;

  /**
   * Creates an instance of a TaskList.
   *
   * @param listName Name of the task list
   */
  public TaskList(String listName) {
    this.listName = listName;
    this.tasks = FXCollections.observableArrayList();
  }

  /**
   * Set a new name for the task list.
   *
   * @param newName new name for the task list
   */
  public void setListName(String newName) {
    this.listName = newName;
  }


  /**
   * Returns the name of the task list.
   *
   * @return returns the name of the task list
   */
  public String getListName() {
    return this.listName;
  }

  /**
   * Returns a list of tasks.
   *
   * @return the list of tasks
   */
  public ObservableList<Task> getTasks() {
    return this.tasks;
  }

  /**
   * Adds a task to the task list.
   *
   * @param task the task to add to the task list
   */
  public void addTask(Task task) {
    this.tasks.add(task);
  }

  /**
   * Removes a task from the task list.
   *
   * @param task task to be removed
   */
  public void removeTask(Task task) {
    this.tasks.remove(task);
  }

  @Override
  public String toString() {
    return "Task list: " + getListName();
  }
}
