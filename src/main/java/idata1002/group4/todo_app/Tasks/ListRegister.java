package idata1002.group4.todo_app.Tasks;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Represents a register holding a list of task lists.
 */
public class ListRegister {
  private final List<TaskList> lists;

  /**
   * Creat an instance of list register.
   */
  public ListRegister() {
    this.lists = new ArrayList<>();
  }

  /**
   * Add a task list to the register.
   *
   * @param list Task list to be added
   */
  public void addList(TaskList list) {
    this.lists.add(list);
  }

  /**
   * Returns the list register.
   *
   * @return List register
   */
  public List<TaskList> getLists() {
    return this.lists;
  }

  /**
   * Finds a list with name matching the input and returns the list found.
   *
   * @param listName Name of list to be found
   * @return List with matching name
   */
  public TaskList findList(String listName) {
    TaskList foundList;

    List<TaskList> list = this.lists.stream().filter(c -> c.getListName().equals(listName))
        .collect(Collectors.toList());

    foundList = list.get(0);

    return foundList;
  }
}
