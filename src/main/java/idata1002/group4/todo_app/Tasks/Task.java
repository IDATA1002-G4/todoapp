package idata1002.group4.todo_app.Tasks;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;

/**
 * This class holds the information about a task.
 * Name, date created, priority and the status
 */
public class Task {
  private String taskName;
  private final LocalDate dateCreated;
  private LocalDate endDate;
  private boolean done;

  /**
   * Creates an instance of Task.
   *
   * @param taskName The name of the task
   */
  public Task(String taskName) {
    this.taskName = taskName;
    this.dateCreated = LocalDate.now();
    this.endDate = null;
    this.done = false;
  }

  /**
   * Creates an instance of Task with set end date.
   *
   * @param taskName The name of the task, not null
   * @param endDate  The end date in this format "2021-03-16", not null
   */
  public Task(String taskName, String endDate) {
    this.taskName = taskName;
    this.dateCreated = LocalDate.now();
    this.endDate = LocalDate.parse(endDate);
    this.done = false;
  }

  /**
   * Format the date to correct format for localdate.
   */
  private void dateFormater(String date) throws ParseException {
    String formattedDate;
    Date parsedDate = new SimpleDateFormat("dd.MM.yyyy").parse(date);
    formattedDate = new SimpleDateFormat("yyyy-MM-dd").format(parsedDate);

    String dateString = String.format(date, "yyyy-MM-dd");
  }

  /**
   * Returns the name of the task.
   *
   * @return The name of the task
   */
  public String getTaskName() {
    return this.taskName;
  }

  /**
   * Returns the date the task was created.
   *
   * @return The date the task was created
   */
  public LocalDate getDateCreated() {
    return this.dateCreated;
  }

  /**
   * Returns the tasks end date.
   *
   * @return The end date of the task
   */
  public LocalDate getEndDate() {
    return this.endDate;
  }

  /**
   * Returns the status of the task.
   *
   * @return returns <code>false</code> if its not done and <code>true</code> if it is done
   */
  public boolean getDone() {
    return this.done;
  }

  /**
   * Change the name of the task.
   *
   * @param newName The new name of the task
   */
  public void setTaskName(String newName) {
    this.taskName = newName;
  }

  /**
   * Change the end date of the task.
   *
   * @param endDate The enw end date "2021-03-16"
   */
  public void setEndDate(String endDate) {
    this.endDate = LocalDate.parse(endDate);
  }

  /**
   * Change the status of the task.
   */
  public void changeStatus() {
    if (done) {
      this.done = false;
    } else {
      this.done = true;
    }
  }

  @Override
  public String toString() {
    return "Task: " + getTaskName() + " Created: " + getDateCreated() + " Status: " + getDone();
  }
}
