module idata1002.group4.todo_app {
    requires javafx.controls;
    requires javafx.fxml;
    requires org.junit.jupiter.api;

    opens idata1002.group4.todo_app.Controller to javafx.fxml;
    exports idata1002.group4.todo_app.Controller;
    exports idata1002.group4.todo_app.Tasks;
    opens idata1002.group4.todo_app.Tasks to javafx.fxml;
    exports idata1002.group4.todo_app.App;
    opens idata1002.group4.todo_app.App to javafx.fxml;

}