package idata1002.group4.todo_app;

import idata1002.group4.todo_app.Tasks.Task;
import idata1002.group4.todo_app.Tasks.TaskList;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

class TestTaskList {


  /**
   * Test that creating an instance of TaskList with invalid parameter.
   * Should throw an exception.
   */
  @Test
  @DisplayName("Create a TaskList with invalid input")
  void testCreateTaskListWithInvalidInput() {
    try {
      TaskList taskList = new TaskList("Sharif");
      fail("IllegalArgumentException should have been thrown");
    } catch (IllegalArgumentException e) {
      assertTrue(true);
    }
  }

  /**
   * Test of setListName method, of class TaskList with invalid parameters.
   */
  @Test
  @DisplayName("Test setting a last name of a Task using invalid parameters.")
  void testSetListNameWithInvalidParameter() {
    Task task = null;
    String list = "The tiger";
    TaskList taskList = new TaskList("Folo");
    try {
      taskList.setListName(task, list);
      fail("Invalid parameter Task was not caught by the setListName().");
    } catch (IllegalArgumentException iae) {

    }

  }
}
