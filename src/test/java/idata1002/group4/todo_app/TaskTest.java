package idata1002.group4.todo_app;


import idata1002.group4.todo_app.Tasks.Task;
import org.junit.jupiter.api.*;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

/**
 * This is the test class for Task
 */
<<<<<<< HEAD
public class TaskTest{

    /**
     *
     * Test the method for creating a task
     */
    @Test
    public void testCreatTask(){

        Task task = new Task("Rydde rommet");
        assertEquals("Rydde rommet", task.getTaskName());
    }

    /**
     *
     * Test the method for creating a task with null as taskName
     */
    @Test
    public void testCreatNullTask(){

        Task task = new Task(null);
        assertEquals(null, task.getTaskName());
    }

    /**
     * Test the method of creating a task with end date
     */
    @Test
    public void testCreatTestWithEndDate(){
        Task task = new Task("Gjøre ferdig oppgave 1.", "2021-03-18");
        assertEquals("Gjøre ferdig oppgave 1.", task.getTaskName());
        assertEquals(LocalDate.parse("2021-03-18"), task.getEndDate());
    }

    /**
     * Test the change end date methode
     */
    @Test
    public void testChangeEndDate(){
        Task task = new Task("Gjøre ferdig oppgave 1.", "2021-03-18");
        assertEquals(LocalDate.parse("2021-03-18"), task.getEndDate());
        task.setEndDate("2222-02-22");
        assertEquals(LocalDate.parse("2222-02-22"), task.getEndDate());
    }

    /**
     * Test the method for changing status
     */
    @Test
    public void testChangeStatus(){
        Task task = new Task("Rydde rommet");
        assertFalse(task.getDone());
        task.changeStatus();
        assertTrue(task.getDone());
    }

    /**
     * test the methode to change the task name
     */
    @Test
    public void testChangeTaskName(){
        Task task = new Task("Rydde rommet");
        task.setTaskName("Jeg vill ikke gjøre tester");
        assertEquals("Jeg vill ikke gjøre tester", task.getTaskName());
    }

=======
class TaskTest {


  /**
   * Test the method for creating a task
   */
  @Test
  void testCreatTask() {

    Task task = new Task("Rydde rommet");
    assertEquals("Rydde rommet", task.getTaskName());
  }

  /**
   * Test the method for creating a task with null as taskName
   */
  @Test
  void testCreatNullTask() {

    Task task = new Task(null);
    assertEquals(null, task.getTaskName());
  }

  /**
   * Test the method of creating a task with end date
   */
  @Test
  void testCreatTestWithEndDate() {
    Task task = new Task("Gjøre ferdig oppgave 1.", "2021-03-18");
    assertEquals("Gjøre ferdig oppgave 1.", task.getTaskName());
    assertEquals(LocalDate.parse("2021-03-18"), task.getEndDate());
  }

  /**
   * Test the change end date methode
   */
  @Test
  void testChangeEndDate() {
    Task task = new Task("Gjøre ferdig oppgave 1.", "2021-03-18");
    assertEquals(LocalDate.parse("2021-03-18"), task.getEndDate());
    task.setEndDate("2222-02-22");
    assertEquals(LocalDate.parse("2222-02-22"), task.getEndDate());
  }

  /**
   * Test the method for changing status
   */
  @Test
  void testChangeStatus() {
    Task task = new Task("Rydde rommet");
    assertFalse(task.getDone());
    task.changeStatus();
    assertTrue(task.getDone());
  }

  /**
   * test the methode to change the task name
   */
  @Test
  void testChangeTaskName() {
    Task task = new Task("Rydde rommet");
    task.setTaskName("Jeg vill ikke gjøre tester");
    assertEquals("Jeg vill ikke gjøre tester", task.getTaskName());
  }
>>>>>>> bb7b4d9238f22060e0c2ba4abe24a7856fcb3443


}